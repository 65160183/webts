function printStatusCode(code: string|number){
    if(typeof code === 'string'){
        console.log(`My status code is ${code.toUpperCase()} type ${typeof code}`); 
    }else {
        console.log(`My status code is ${code} type ${typeof code}`); 
    }
}

printStatusCode(404);
printStatusCode("EEQQ");